package com.koralat.meetingplanner.support.to;

import java.util.Date;
import java.util.List;


public class ReunionTO {
	private String id;
	private String motivo;
	private String organizador;
	private Date fechaPlaneada;
	private long duracion;
	private String locacion;
	private String recursos;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public String getOrganizador() {
		return organizador;
	}
	public void setOrganizador(String organizador) {
		this.organizador = organizador;
	}
	public Date getFechaPlaneada() {
		return fechaPlaneada;
	}
	public void setFechaPlaneada(Date fechaPlaneada) {
		this.fechaPlaneada = fechaPlaneada;
	}
	public long getDuracion() {
		return duracion;
	}
	public void setDuracion(long duracion) {
		this.duracion = duracion;
	}
	public String getLocacion() {
		return locacion;
	}
	public void setLocacion(String locacion) {
		this.locacion = locacion;
	}
	public String getRecursos() {
		return recursos;
	}
	public void setRecursos(String recursos) {
		this.recursos = recursos;
	}
}
