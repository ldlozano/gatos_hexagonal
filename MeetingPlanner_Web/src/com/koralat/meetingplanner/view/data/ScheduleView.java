package com.koralat.meetingplanner.view.data;


import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.LazyScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

import com.koralat.meetingplanner.business.bc.ReunionBusiness;
import com.koralat.meetingplanner.business.bc.impl.ReunionBusinessDefault;
import com.koralat.meetingplanner.support.to.ReunionTO;
import com.koralat.meetingplanner.support.to.SemanaTO;
 
@ManagedBean
@ViewScoped
public class ScheduleView implements Serializable {
 
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ScheduleModel reunionModel;
     
    private ScheduleModel lazyEventModel;
 
    private ScheduleEvent event = new DefaultScheduleEvent();
 
    @PostConstruct
    public void init() {
        reunionModel = new DefaultScheduleModel();
        ReunionBusiness rb = new ReunionBusinessDefault();
        SemanaTO semana = new SemanaTO();
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        semana.setInicio(c.getTime());
        c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        semana.setFin(c.getTime());
        List<ReunionTO> reuniones = rb.verReuniones(semana);
        for (ReunionTO reunionTO : reuniones) {
			ScheduleEvent event = new DefaultScheduleEvent("",reunionTO.getFechaPlaneada(),reunionTO.getFechaPlaneada());
			event.setId(reunionTO.getId());
            reunionModel.addEvent(event);
		}
    }
     
    public Date getInitialDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), Calendar.FEBRUARY, calendar.get(Calendar.DATE), 0, 0, 0);
        return calendar.getTime();
    }
     
    public ScheduleModel getEventModel() {
        return reunionModel;
    }
     
    public ScheduleEvent getEvent() {
        return event;
    }
 
    public void setEvent(ScheduleEvent event) {
        this.event = event;
    }
     
    public void addEvent(ActionEvent actionEvent) {
    	ReunionBusiness rb = new ReunionBusinessDefault();
    	ReunionTO reunion = new ReunionTO();
        if(event.getId() == null){
        	reunion.setId(event.getId());
        	reunion.setMotivo(event.getDescription());
        	reunion.setFechaPlaneada(event.getStartDate());
        	reunion.setDuracion(1L);
        	rb.nuevaReunion(reunion);
        }
        else{
            rb.moverReunion(reunion);
        }
        event = new DefaultScheduleEvent();
    }
    
    
     
    public void onEventSelect(SelectEvent selectEvent) {
        event = (ScheduleEvent) selectEvent.getObject();
    }
     
    public void onDateSelect(SelectEvent selectEvent) {
        event = new DefaultScheduleEvent("", (Date) selectEvent.getObject(), (Date) selectEvent.getObject());
    }
     
    public void onEventMove(ScheduleEntryMoveEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event moved", "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());
         
        addMessage(message);
    }
     
    public void onEventResize(ScheduleEntryResizeEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event resized", "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());
         
        addMessage(message);
    }
     
    private void addMessage(FacesMessage message) {
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
}