package co.koralat.meetingplanner.db.mongo.spring.entity;

import java.util.List;

import org.springframework.data.annotation.Id;

public class ReunionEntity {
	@Id
	private String id;
	private List<PersonaEntity> asistentes;
	private CalendarioEntity ocurrencia;
	private String sala;
	private List<String> recursosNecesarios;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<PersonaEntity> getAsistentes() {
		return asistentes;
	}
	public void setAsistentes(List<PersonaEntity> asistentes) {
		this.asistentes = asistentes;
	}
	public CalendarioEntity getOcurrencia() {
		return ocurrencia;
	}
	public void setOcurrencia(CalendarioEntity ocurrencia) {
		this.ocurrencia = ocurrencia;
	}
	public String getSala() {
		return sala;
	}
	public void setSala(String sala) {
		this.sala = sala;
	}
	public List<String> getRecursosNecesarios() {
		return recursosNecesarios;
	}
	public void setRecursosNecesarios(List<String> recursosNecesarios) {
		this.recursosNecesarios = recursosNecesarios;
	}
}
