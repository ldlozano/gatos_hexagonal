package co.koralat.meetingplanner.db.mongo.spring.entity;

import org.springframework.data.annotation.Id;

public class PersonaEntity {
	@Id
	private String id;
	private String nombre;
	private AgendaEntity agenda;
	
	public PersonaEntity(String id, String nombre, AgendaEntity agenda) {
		this.id = id;
		this.nombre = nombre;
		this.agenda = agenda;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public AgendaEntity getAgenda() {
		return agenda;
	}
	
}
