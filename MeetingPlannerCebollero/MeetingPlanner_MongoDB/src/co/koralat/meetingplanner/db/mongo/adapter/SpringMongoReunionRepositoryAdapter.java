package co.koralat.meetingplanner.db.mongo.adapter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import co.koralat.meeting.calendario.domain.entities.Calendario;
import co.koralat.meeting.persona.domain.entities.Agenda;
import co.koralat.meeting.persona.domain.entities.Persona;
import co.koralat.meeting.reunion.domain.entities.Reunion;
import co.koralat.meeting.reunion.domain.repository.ReunionRepository;
import co.koralat.meetingplanner.db.mongo.spring.MongoReunionRepository;
import co.koralat.meetingplanner.db.mongo.spring.entity.AgendaEntity;
import co.koralat.meetingplanner.db.mongo.spring.entity.PersonaEntity;
import co.koralat.meetingplanner.db.mongo.spring.entity.ReunionEntity;

public class SpringMongoReunionRepositoryAdapter implements ReunionRepository{
	

	@Autowired
	private MongoReunionRepository repository;
	
	public Reunion buscarPorId(String id) {
		ReunionEntity reunion = repository.findOne(id);
		Calendario calendar = new Calendario(reunion.getOcurrencia().getInicio(),reunion.getOcurrencia().getDuracion());
		List<Persona> asistentes = new ArrayList<Persona>();
		List<PersonaEntity> persistAsistente = reunion.getAsistentes();
		for (PersonaEntity personaEntity : persistAsistente) {
			Agenda agenda = new Agenda();
			Persona persona = new Persona(personaEntity.getId(), personaEntity.getNombre(), agenda);
			asistentes.add(persona);
		}
		Reunion result = new Reunion(id, asistentes , calendar, reunion.getSala(), reunion.getRecursosNecesarios());
		return result;
	}
	@Override
	public List<Reunion> traerTodas() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public int guardar(
			Reunion reunion) {
		ReunionEntity reunionEntity = new ReunionEntity();
		reunionEntity.setId(reunion.getId());
		reunionEntity.getRecursosNecesarios().addAll(reunion.getRecursosNecesarios());
		
		List<PersonaEntity> asistentes = new ArrayList<PersonaEntity>();
		List<Persona> persistAsistente = reunion.getAsistentes();
		for (Persona persona : persistAsistente) {
			AgendaEntity agenda = new AgendaEntity();
			PersonaEntity personaEntity = new PersonaEntity(persona.getId(), persona.getNombre(), agenda);
			asistentes.add(personaEntity);
		}
		
		reunionEntity.setAsistentes(asistentes);		
		repository.save(reunionEntity);
		return 0;
	}

	
}
