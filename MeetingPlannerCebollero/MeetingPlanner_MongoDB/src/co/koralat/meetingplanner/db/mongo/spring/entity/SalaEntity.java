package co.koralat.meetingplanner.db.mongo.spring.entity;

import java.util.List;

import org.springframework.data.annotation.Id;

public class SalaEntity {
	@Id
	private String id;
	private List<RecursoEntity> recursos;
	private int capacidad;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<RecursoEntity> getRecursos() {
		return recursos;
	}
	public void setRecursos(List<RecursoEntity> recursos) {
		this.recursos = recursos;
	}
	public int getCapacidad() {
		return capacidad;
	}
	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}
}
