package co.koralat.meetingplanner.db.mongo.spring;

import org.springframework.data.mongodb.repository.MongoRepository;

import co.koralat.meetingplanner.db.mongo.spring.entity.ReunionEntity;

public interface MongoReunionRepository extends MongoRepository<ReunionEntity, String> {
	
}
