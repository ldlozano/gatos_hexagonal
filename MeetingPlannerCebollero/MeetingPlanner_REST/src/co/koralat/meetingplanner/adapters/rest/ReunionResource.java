package co.koralat.meetingplanner.adapters.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import co.koralat.meeting.application.ReunionService;
import co.koralat.meeting.application.to.ReunionTO;

@Path("/reuniones")
public class ReunionResource {
	
	ReunionService reunionService;
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(ReunionTO reunion){
		reunionService.crearReunion(reunion);
		Response response = Response.ok().build();
		return response;
	}
	
}
