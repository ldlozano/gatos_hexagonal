package co.koralat.meeting.calendario.domain.entities;

public class Horario {
	private DiaSemana dia;
	private double horaInicio;
	private double horaFin;
	private Repeticion repetir;
	
	public Horario(DiaSemana dia, double horaInicio, double horaFin, Repeticion repeticion ) {
		this.dia = dia;
		this.horaFin = horaFin;
		this.horaInicio = horaInicio;
		this.repetir = repeticion;
	}
	
	public double getHoraInicio() {
		return horaInicio;
	}
	public double getHoraFin() {
		return horaFin;
	}
	public Repeticion getRepetir() {
		return repetir;
	}
}
