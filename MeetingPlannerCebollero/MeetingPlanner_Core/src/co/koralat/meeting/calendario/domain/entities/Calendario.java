package co.koralat.meeting.calendario.domain.entities;

import java.sql.Date;

public class Calendario {
	private Date inicio;
	private double duracion;
	
	public Calendario(Date inicio, double duracion) {
		this.inicio = inicio;
		this.duracion = duracion;
	}

	public Date getInicio() {
		return inicio;
	}

	public double getDuracion() {
		return duracion;
	}
	
}
