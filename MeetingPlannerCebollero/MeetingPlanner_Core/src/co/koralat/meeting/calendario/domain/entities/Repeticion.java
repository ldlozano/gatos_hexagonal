package co.koralat.meeting.calendario.domain.entities;

import java.sql.Date;

public class Repeticion {
	private Date inicio;
	private Date fin;
	private TipoRepeticion tipo;
	
	public Date getInicio() {
		return inicio;
	}

	public Date getFin() {
		return fin;
	}

	public TipoRepeticion getTipo() {
		return tipo;
	}

	
	public Repeticion(Date desde, Date hasta, TipoRepeticion tipo) {
		this.inicio = desde;
		this.fin = hasta;
		this.tipo = tipo;
	}
	
	public Repeticion(Date desde, int ocurrencias, TipoRepeticion tipo) {
		this.inicio = desde;
		this.fin = calcularFinalizacion(desde,ocurrencias, tipo);
	}
	
	private Date calcularFinalizacion(Date inicio, int ocurrencias, TipoRepeticion tipo){
		Date fin = inicio;
		switch (tipo) {
		case DIARIA:
			
			break;
		case MENSUAL:
			break;
		case SEMANAL:
			break;
		case QUINCENAL:
			break;
		default:
			break;
		}
		return fin;
	}
	
}
