package co.koralat.meeting.calendario.domain.entities;

public enum TipoRepeticion {
		DIARIA(1), SEMANAL(2), QUINCENAL(3), MENSUAL (4);
		private int value;
		private TipoRepeticion(int value) {
			this.setValue(value);
		}
		public int getValue() {
			return value;
		}
		private void setValue(int value) {
			this.value = value;
		}
}
