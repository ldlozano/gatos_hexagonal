package co.koralat.meeting.persona.domain.repository;

import co.koralat.meeting.calendario.domain.entities.Calendario;

public interface AgendaRepository {
	public Calendario getAgenda(String personaId);
}
