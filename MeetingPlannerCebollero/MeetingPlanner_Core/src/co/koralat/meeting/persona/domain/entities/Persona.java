package co.koralat.meeting.persona.domain.entities;



public class Persona {
	public String getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

	public Agenda getAgenda() {
		return agenda;
	}

	private String id;
	private String nombre;
	private Agenda agenda;
	
	public Persona(String id, String nombre,Agenda agenda) {
		this.id = id;
		this.nombre = nombre;
		
	}
}
