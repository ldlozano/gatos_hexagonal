package co.koralat.meeting.persona.domain.repository;

import java.util.List;

import co.koralat.meeting.persona.domain.entities.Persona;

public interface PersonaRepository {
	public int save(Persona persona);
	public Persona buscarPersonaPorNombre(String nombre);
	public List<Persona> buscarPersonas(List<String> asistentes);
	
}
