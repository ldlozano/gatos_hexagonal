package co.koralat.meeting.application;

import co.koralat.meeting.application.to.AsistenteTO;
import co.koralat.meeting.application.to.RespuestaCrearReunionTO;
import co.koralat.meeting.application.to.ReunionTO;

public interface ReunionService {
	public RespuestaCrearReunionTO crearReunion(ReunionTO reunion);
	public void adicionarAsistente(AsistenteTO asistente);
}
