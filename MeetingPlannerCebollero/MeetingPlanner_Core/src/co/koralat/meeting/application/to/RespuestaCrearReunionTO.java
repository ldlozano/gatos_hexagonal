package co.koralat.meeting.application.to;

import java.sql.Date;

public class RespuestaCrearReunionTO {
	private String id;
	private Date fecha;
	
	public RespuestaCrearReunionTO(String id, Date fecha) {
		this.id = id;
		this.fecha = fecha;
	}

	public String getId() {
		return id;
	}

	public Date getFecha() {
		return fecha;
	}
}

