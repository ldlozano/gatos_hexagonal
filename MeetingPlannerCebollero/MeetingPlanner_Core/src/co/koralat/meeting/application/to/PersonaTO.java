package co.koralat.meeting.application.to;

public class PersonaTO {
	String id;
	String nombre; 
	public PersonaTO(String id, String nombre) {
		this.id = id;
		this.nombre = nombre;
	}
	public String getId() {
		return id;
	}
	public String getNombre() {
		return nombre;
	}
}
