package co.koralat.meeting.application.to;

import java.sql.Date;
import java.util.List;

public class ReunionTO {
	private Date fecha;
	private double duracion;
	private List<String> recursos;
	private List<String> asistentes;
	
	public ReunionTO(Date fecha, double duracion, List<String> recursos, List<String> asistentes) {
		this.fecha = fecha;
		this.duracion = duracion;
		this.recursos.addAll(recursos);
		this.asistentes.addAll(asistentes);
	}
	
	public Date getFecha() {
		return fecha;
	}
	public double getDuracion() {
		return duracion;
	}
	public List<String> getRecursos() {
		return recursos;
	}
	
	public List<String> getAsistentes(){
		return asistentes;
	}
	
	
}
