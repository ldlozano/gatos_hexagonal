package co.koralat.meeting.application.to;

public class AsistenteTO {
	public String getReunionId() {
		return reunionId;
	}

	public String getPersonaId() {
		return personaId;
	}

	private String reunionId;
	private String personaId;
	
	public AsistenteTO(String idReunion, String idPersona) {
		this.reunionId = idReunion;
		this.personaId = idPersona;
	}
	
	
	
}
