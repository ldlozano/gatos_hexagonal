package co.koralat.meeting.application.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import co.koralat.meeting.application.ReunionService;
import co.koralat.meeting.application.to.AsistenteTO;
import co.koralat.meeting.application.to.RespuestaCrearReunionTO;
import co.koralat.meeting.application.to.ReunionTO;
import co.koralat.meeting.calendario.domain.entities.Calendario;
import co.koralat.meeting.persona.domain.entities.Persona;
import co.koralat.meeting.persona.domain.repository.PersonaRepository;
import co.koralat.meeting.reunion.domain.entities.Reunion;
import co.koralat.meeting.reunion.domain.repository.ReunionRepository;

public class ReunionServiceImpl implements ReunionService {
	
	@Autowired
	ReunionRepository reunionRepository;
	
	@Autowired
	PersonaRepository personaRepository;
	
	@Override
	public RespuestaCrearReunionTO crearReunion(ReunionTO reunionTO) {
		
		List<Persona> asistentes = this.personaRepository.buscarPersonas(reunionTO.getAsistentes());
		Calendario ocurrencia = new Calendario(reunionTO.getFecha(), reunionTO.getDuracion());
		
		
		Reunion reunion = new Reunion(asistentes, ocurrencia, reunionTO.getRecursos());
		reunion.planear();
		this.reunionRepository.guardar(reunion);
		RespuestaCrearReunionTO respuesta = new RespuestaCrearReunionTO(reunion.getId(), reunion.getOcurrencia().getInicio());
		return respuesta;
	}

	@Override
	public void adicionarAsistente(AsistenteTO asistente) {
		Reunion reunion = this.reunionRepository.buscarPorId(asistente.getReunionId());
		Persona nuevoAsistente = this.personaRepository.buscarPersonaPorNombre(asistente.getPersonaId());
		reunion.adicionarAsistente(nuevoAsistente);
		this.reunionRepository.guardar(reunion);
	}
	
	public void setReunionRepository(ReunionRepository reunionRepository) {
		this.reunionRepository = reunionRepository;
	}

	public void setPersonaRepository(PersonaRepository personaRepository) {
		this.personaRepository = personaRepository;
	}
	
	
	
}
