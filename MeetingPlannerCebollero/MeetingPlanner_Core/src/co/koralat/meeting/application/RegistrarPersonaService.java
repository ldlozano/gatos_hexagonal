package co.koralat.meeting.application;

import co.koralat.meeting.application.to.PersonaTO;

public interface RegistrarPersonaService {
	public void registrar(PersonaTO persona);
}
