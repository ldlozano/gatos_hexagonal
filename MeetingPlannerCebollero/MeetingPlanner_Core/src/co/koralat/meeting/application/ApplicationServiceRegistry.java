package co.koralat.meeting.application;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class ApplicationServiceRegistry implements ApplicationContextAware {
	private static ApplicationContext applicationContext;

	public static RegistrarPersonaService registrarPersonaService() {
		return (RegistrarPersonaService) applicationContext
				.getBean("registrarPersonaService");
	}

	@Override
	public synchronized void setApplicationContext(
			ApplicationContext anApplicationContext) throws BeansException {
		if (ApplicationServiceRegistry.applicationContext == null) {
			ApplicationServiceRegistry.applicationContext = anApplicationContext;
		}
	}
}