package co.koralat.meeting.reunion.domain.entities;

import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import co.koralat.meeting.calendario.domain.entities.Calendario;
import co.koralat.meeting.locaciones.domain.entities.Recurso;
import co.koralat.meeting.locaciones.domain.entities.Sala;
import co.koralat.meeting.persona.domain.entities.Persona;

public class Reunion {
	public String getId() {
		return id;
	}

	public List<Persona> getAsistentes() {
		return asistentes;
	}

	public Calendario getOcurrencia() {
		return ocurrencia;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public List<String> getRecursosNecesarios() {
		return recursosNecesarios;
	}

	private String id;
	private List<Persona> asistentes;
	private Calendario ocurrencia;
	private String ubicacion;
	private List<String> recursosNecesarios;
	
	public Reunion(List<Persona> asistentes, Calendario ocurrencia, List<String> recursosNecesarios ) {
		generateId();
		this.asistentes.addAll(asistentes);
		this.ocurrencia = ocurrencia;
		this.recursosNecesarios.addAll(recursosNecesarios);
	}
	
	public Reunion(String id, List<Persona> asistentes, Calendario ocurrencia, String ubicacion, List<String> recursosNecesarios ) {
		this.id = id;
		this.asistentes.addAll(asistentes);
		this.ocurrencia = ocurrencia;
		this.ubicacion = ubicacion;
		this.recursosNecesarios.addAll(recursosNecesarios);
	}
	
	private void generateId(){
		UUID uuid = UUID.randomUUID();
		this.id = uuid.toString();
	}
	
	public void planear(){
	  	
		
	}

	public void adicionarAsistente(Persona nuevoAsistente) {
		this.asistentes.add(nuevoAsistente);
	}
	
	
	
}
