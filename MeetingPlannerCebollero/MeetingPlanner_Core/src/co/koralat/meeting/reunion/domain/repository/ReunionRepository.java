package co.koralat.meeting.reunion.domain.repository;

import java.util.List;

import co.koralat.meeting.reunion.domain.entities.Reunion;

public interface ReunionRepository {
	public Reunion buscarPorId(String id);
	public List<Reunion> traerTodas();
	public int guardar(Reunion reunion);
}
