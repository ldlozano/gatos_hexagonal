package co.koralat.meeting.locaciones.domain.entities;

import java.util.List;

public class Sala {
	private String id;
	private List<Recurso> recursos;
	private int capacidad;

	public Sala(String id, List<Recurso> recursos, int capacidad) {
		this.id = id;
		this.recursos.addAll(recursos);
		this.capacidad = capacidad;
	}

	public String getId() {
		return id;
	}

	public List<Recurso> getRecursos() {
		return recursos;
	}

	public int getCapacidad() {
		return capacidad;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		Sala comp = (Sala) obj;
		if (this.id == comp.id)
			return true;
		return false;
	}

}
