package co.koralat.meeting.locaciones.domain.entities;

public class Recurso {
	private String id;
	private String nombre;
	
	public Recurso(String pId, String pNombre) {
		this.id = pId;
		this.nombre = pNombre;
	}
	
	public String getId() {
		return id;
	}
	public String getNombre() {
		return nombre;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		Recurso comp = (Recurso) obj;
		if(this.id == comp.id)
			return true;
		return false;
	}
	
}
