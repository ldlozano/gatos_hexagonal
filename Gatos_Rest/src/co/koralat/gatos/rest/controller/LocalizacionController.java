package co.koralat.gatos.rest.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

import co.koralat.gatos.localizacion.core.applicacion.LocalizacionService;
import co.koralat.gatos.localizacion.core.applicacion.impl.LocalizacionServiceImpl;
import co.koralat.gatos.localizacion.soporte.exception.NoTieneGatosException;
import co.koralat.gatos.localizacion.soporte.to.CuidadorTO;


@Path("/localizacion")
public class LocalizacionController {

	@Path("/crearcuidador")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public void crearCuidador(CuidadorTO cuidador){
		LocalizacionService localizacion = new LocalizacionServiceImpl();
		try {
			localizacion.crearCuidador(cuidador);
		} catch (NoTieneGatosException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
