package test.co.koralat.gatos.localizacion.core.entities;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import co.koralat.gatos.localizacion.core.entities.Cuidador;
import co.koralat.gatos.localizacion.core.entities.Gato;
import co.koralat.gatos.localizacion.core.vo.Posicion;

public class CuidadorTest {

	@Test
	public void probarCalculo() {
		Posicion posicionCuidador = new Posicion(6.20122,-75.57843);
		Posicion posicionGato = new Posicion(6.236276,-75.580522);
		
		double distanciaEsperada = 3.905;
		
		List<Gato> gatos = new ArrayList<Gato>();
		Gato gatoPerdido = new Gato("misifu", "negro", "ojito gacho", "esfinge", "asexuado", "123456", posicionGato);
		gatos.add(gatoPerdido);
		
		Cuidador cuidador = new Cuidador("cualquiercosa", "Fulanito", gatos, posicionCuidador);
		double distanciaCalculada = cuidador.calcularDistancia(gatoPerdido);
		
		assertEquals(distanciaCalculada, distanciaEsperada, 0.1);
		
	}

	public void probarCalculoMismaPosicion() {
		fail("Not yet implemented");
	}

	
}
