package co.koralat.gatos.localizacion.soporte.exception;

public class NoTieneGatosException extends Exception {

	public NoTieneGatosException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NoTieneGatosException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public NoTieneGatosException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public NoTieneGatosException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public NoTieneGatosException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
