package co.koralat.gatos.localizacion.soporte.to;

import java.util.List;

public class CuidadorTO {
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	private String id;
	private String nombre;
	private String posicion; // Latitud y Longitud separado por comas

	private List<String> gatos; // el codigo de barras del gato

	public String getPosicion() {
		return posicion;
	}

	public void setPosicion(String posicion) {
		this.posicion = posicion;
	}

	public List<String> getGatos() {
		return gatos;
	}

	public void setGatos(List<String> gatos) {
		this.gatos = gatos;
	}

}
