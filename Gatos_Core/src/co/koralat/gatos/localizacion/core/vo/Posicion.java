package co.koralat.gatos.localizacion.core.vo;

public class Posicion {
	public double getLatitud() {
		return latitud;
	}
	public double getLongitud() {
		return longitud;
	}
	public Posicion(double latitud, double longitud) {
		super();
		this.latitud = latitud;
		this.longitud = longitud;
	}
	private double latitud;
	private double longitud;
}
