package co.koralat.gatos.localizacion.core.applicacion;

import co.koralat.gatos.localizacion.soporte.exception.NoTieneGatosException;
import co.koralat.gatos.localizacion.soporte.to.CuidadorTO;
import co.koralat.gatos.localizacion.soporte.to.GatoTO;

public interface LocalizacionService {
	public CuidadorTO crearCuidador(CuidadorTO cuidador) throws NoTieneGatosException;
	public void adicionarGato(CuidadorTO cuidador, GatoTO gato);
	public double calcularDistancia(CuidadorTO cuidador, GatoTO gato);
}
