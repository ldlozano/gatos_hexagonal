package co.koralat.gatos.localizacion.core.applicacion.impl;

import java.util.List;

import co.koralat.gatos.localizacion.core.applicacion.LocalizacionService;
import co.koralat.gatos.localizacion.core.entities.Cuidador;
import co.koralat.gatos.localizacion.core.entities.CuidadorRepository;
import co.koralat.gatos.localizacion.core.entities.Gato;
import co.koralat.gatos.localizacion.core.vo.Posicion;
import co.koralat.gatos.localizacion.soporte.exception.NoTieneGatosException;
import co.koralat.gatos.localizacion.soporte.to.CuidadorTO;
import co.koralat.gatos.localizacion.soporte.to.GatoTO;

public class LocalizacionServiceImpl implements LocalizacionService {

	
	CuidadorRepository repositorioCuidador;
	
	@Override
	public CuidadorTO crearCuidador(CuidadorTO cuidadorTO) throws NoTieneGatosException {
		
		Cuidador cuidadorEntity;
		
		//Obtener los gatos a partir de la lista desde la base de datos
		
		List<Gato> gatos = repositorioCuidador.traerGatos(cuidadorTO.getGatos());
		
		//Crear la posición del tipo, a partir de un string separador por comas
		
		String posicionIngresada = cuidadorTO.getPosicion();
		String[] posicionPartida =  posicionIngresada.split(",");
		double latitud = Double.valueOf(posicionPartida[0]).doubleValue(); 
		double longitud = Double.valueOf(posicionPartida[1]).doubleValue(); 
		
		Posicion posicionCuidador = new Posicion(latitud,longitud);
		
		cuidadorEntity = new Cuidador(cuidadorTO.getId(), cuidadorTO.getNombre(), gatos, posicionCuidador);
		
		cuidadorEntity.validar();
		
		repositorioCuidador.crearCuidador(cuidadorEntity);
		
		return cuidadorTO;
	}

	@Override
	public void adicionarGato(CuidadorTO cuidador, GatoTO gato) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public double calcularDistancia(CuidadorTO cuidador, GatoTO gato) {
		
		Cuidador cuidadorEntity = repositorioCuidador.recuperarCuidador(cuidador);
		Gato gatoPerdido = new Gato();
		double distancia = cuidadorEntity.calcularDistancia(gatoPerdido);
		
		
		return distancia;
	}

    public void setRepositorioCuidador(CuidadorRepository injectedRepositorioCuidador){
	   this.repositorioCuidador = injectedRepositorioCuidador;
	}

}
