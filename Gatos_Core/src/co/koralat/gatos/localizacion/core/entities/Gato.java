package co.koralat.gatos.localizacion.core.entities;

import co.koralat.gatos.localizacion.core.vo.Posicion;

public class Gato {
	public String getNombre() {
		return nombre;
	}
	public String getColor() {
		return color;
	}
	public String getRasgos() {
		return rasgos;
	}
	public String getRaza() {
		return raza;
	}
	public String getGenero() {
		return genero;
	}
	public String getCodigoBarras() {
		return codigoBarras;
	}
	public Posicion getUbicacion() {
		return ubicacion;
	}
	
	public Gato(){}
	
	public Gato(String nombre, String color, String rasgos, String raza,
			String genero, String codigoBarras, Posicion ubicacion) {
		super();
		this.nombre = nombre;
		this.color = color;
		this.rasgos = rasgos;
		this.raza = raza;
		this.genero = genero;
		this.codigoBarras = codigoBarras;
		this.ubicacion = ubicacion;
	}
	private String nombre;
	private String color;
	private String rasgos;
	private String raza;
	private String genero;
	private String codigoBarras;
	private Posicion ubicacion;
}
