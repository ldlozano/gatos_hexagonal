package co.koralat.gatos.localizacion.core.entities;

import java.util.List;

import co.koralat.gatos.localizacion.soporte.to.CuidadorTO;
import co.koralat.gatos.localizacion.soporte.to.GatoTO;

public interface CuidadorRepository {
	public void crearCuidador(Cuidador cuidador);
	public void adicionarGato(Cuidador cuidador, Gato gato);
	public Cuidador recuperarCuidador(CuidadorTO cuidadorId);
	public List<Gato> traerGatos(List<String> gatosIds);
}
