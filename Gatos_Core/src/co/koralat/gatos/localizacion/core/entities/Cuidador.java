package co.koralat.gatos.localizacion.core.entities;

import java.util.List;

import co.koralat.gatos.localizacion.core.vo.Posicion;
import co.koralat.gatos.localizacion.soporte.exception.NoTieneGatosException;

public class Cuidador {

	public String getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

	public List<Gato> getGatos() {
		return gatos;
	}

	public Posicion getUbicacion() {
		return ubicacion;
	}

	public Cuidador(String id, String nombre, List<Gato> gatos,
			Posicion ubicacion) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.gatos = gatos;
		this.ubicacion = ubicacion;
	}

	private String id;
	private String nombre;
	private List<Gato> gatos;
	private Posicion ubicacion;

	public double calcularDistancia(Gato gatoPerdido){
		return 0.0;
	}
	
	public void validar() throws NoTieneGatosException{
		if(gatos == null){
			throw new NoTieneGatosException();
		}
		if(gatos.isEmpty()){
			throw new NoTieneGatosException();
		}
	}
	
	
}
