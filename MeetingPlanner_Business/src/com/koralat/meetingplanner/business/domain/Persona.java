package com.koralat.meetingplanner.business.domain;

public class Persona {
	private int codigo;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	private String nombre;

	@Override
	public boolean equals(Object obj) {
		if(obj == null){
			return false;
		}
		if (obj instanceof Persona) {
			Persona compare = (Persona) obj;
			return this.codigo == compare.codigo;
		} else {
			return false;
		}
	}
}
