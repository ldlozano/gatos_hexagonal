package com.koralat.meetingplanner.business.domain;

import java.util.Date;
import java.util.List;


public class Reunion {
	private int id;
	private String motivo;
	private Persona organizador;
	private Date fechaPlaneada;
	private long duracion;
	private Ubicacion locacion;
	private List<Recurso> recursos;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public Persona getOrganizador() {
		return organizador;
	}
	public void setOrganizador(Persona organizador) {
		this.organizador = organizador;
	}
	public Date getFechaPlaneada() {
		return fechaPlaneada;
	}
	public void setFechaPlaneada(Date fechaPlaneada) {
		this.fechaPlaneada = fechaPlaneada;
	}
	public long getDuracion() {
		return duracion;
	}
	public void setDuracion(long duracion) {
		this.duracion = duracion;
	}
	public Ubicacion getLocacion() {
		return locacion;
	}
	public void setLocacion(Ubicacion locacion) {
		this.locacion = locacion;
	}
	
}
