package com.koralat.meetingplanner.business.domain;

import java.util.List;

public class Ubicacion {
	private String nombre;
	private long capacidad;
	private Calendario disponibilidad;
	private List<Recurso> recursos;
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public long getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(long capacidad) {
		this.capacidad = capacidad;
	}

	public Calendario getDisponibilidad() {
		return disponibilidad;
	}

	public void setDisponibilidad(Calendario disponibilidad) {
		this.disponibilidad = disponibilidad;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		
		if(obj instanceof Ubicacion)
			return this.nombre == ((Ubicacion) obj).nombre;
		
		return false;
	}
}
