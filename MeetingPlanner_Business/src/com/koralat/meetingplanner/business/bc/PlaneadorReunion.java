package com.koralat.meetingplanner.business.bc;

import com.koralat.meetingplanner.support.to.CalendarioTO;
import com.koralat.meetingplanner.support.to.ReunionTO;

public interface PlaneadorReunion {
	public CalendarioTO planear(ReunionTO reunion);
}
