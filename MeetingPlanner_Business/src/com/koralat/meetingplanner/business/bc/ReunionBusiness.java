package com.koralat.meetingplanner.business.bc;

import java.util.List;

import com.koralat.meetingplanner.support.to.AsistenciaTO;
import com.koralat.meetingplanner.support.to.ReunionTO;
import com.koralat.meetingplanner.support.to.SemanaTO;

public interface ReunionBusiness {
	public ReunionTO nuevaReunion(ReunionTO reunion);
	public ReunionTO moverReunion(ReunionTO reunion);
	public ReunionTO adicionarAsistente(AsistenciaTO asistencia);
	public List<ReunionTO> verReuniones(SemanaTO semana);
} 
