package com.koralat.meetingplanner.business.bc.impl;

import java.util.Collections;
import java.util.List;

import com.koralat.meetingplanner.business.bc.PlaneadorReunion;
import com.koralat.meetingplanner.business.bc.ReunionBusiness;
import com.koralat.meetingplanner.data.dao.ReunionDAO;
import com.koralat.meetingplanner.data.dao.factory.FabricaDAO;
import com.koralat.meetingplanner.support.to.AsistenciaTO;
import com.koralat.meetingplanner.support.to.ReunionTO;
import com.koralat.meetingplanner.support.to.SemanaTO;

public class ReunionBusinessDefault implements ReunionBusiness {

	@Override
	public ReunionTO nuevaReunion(ReunionTO reunion) {
		FabricaDAO fabrica = new FabricaDAO();
		fabrica.iniciarContextoConexion();
		System.out.println("FabricaDAO....");
		fabrica.iniciarTransaccion();
		ReunionDAO reunionDAO = fabrica.crearReunionDAO();
		int result = reunionDAO.save(reunion);
		PlaneadorReunion planeador = new PlaneadorReunionEstatico();
		planeador.planear(reunion);
		fabrica.finalizarTransaccion();
		fabrica.finalizarContextoConexion();
		return reunion;
	}
	
	
	
	@Override
	public ReunionTO moverReunion(ReunionTO reunion) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ReunionTO adicionarAsistente(AsistenciaTO asistencia) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public List<ReunionTO> verReuniones(SemanaTO semana) {
		List<ReunionTO> result = Collections.emptyList();
		FabricaDAO fabrica = new FabricaDAO();
		fabrica.iniciarContextoConexion();
		fabrica.iniciarTransaccion();
		ReunionDAO reunionDAO = fabrica.crearReunionDAO();
		result = reunionDAO.list();
		fabrica.finalizarTransaccion();
		fabrica.finalizarContextoConexion();
		return result;
	}

}
