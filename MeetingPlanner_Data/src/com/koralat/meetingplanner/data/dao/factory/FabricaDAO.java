package com.koralat.meetingplanner.data.dao.factory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.koralat.meetingplanner.data.dao.ReunionDAO;
import com.koralat.meetingplanner.data.dao.mongo.MongoReunionDAO;

public class FabricaDAO {
	private static String MONGO_URL="jdbc:mongo://localhost:27017/meetingsdb";
	private static String MONGO_DRIVER = "mongodb.jdbc.MongoDriver";
	private ReunionDAO reunionDAO;
	private Connection con;
	
	public void iniciarContextoConexion(){
		try {
			Class.forName(MONGO_DRIVER);
			con = DriverManager.getConnection(MONGO_URL, "meetings_user", "meetings_pwd");
			System.out.println("Se realizo conexion a " + MONGO_URL);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void finalizarContextoConexion(){
		try {
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void iniciarTransaccion(){
		try {
			this.con.setAutoCommit(false);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void finalizarTransaccion(){
		try {
			this.con.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void abortarTransaccion(Exception e){
		try {
			this.con.rollback();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		e.printStackTrace();
	}
	
	public ReunionDAO crearReunionDAO(){
		if (this.reunionDAO == null) {
			this.reunionDAO = new MongoReunionDAO();
			this.reunionDAO.setConexion(this.con);
			System.out.println("Creo ReunionDAO ");
		}
		return this.reunionDAO;
	}
	
	
}
