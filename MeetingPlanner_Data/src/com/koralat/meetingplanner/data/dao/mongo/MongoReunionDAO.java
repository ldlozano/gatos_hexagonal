package com.koralat.meetingplanner.data.dao.mongo;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.koralat.meetingplanner.data.dao.ReunionDAO;
import com.koralat.meetingplanner.support.to.ReunionTO;

public class MongoReunionDAO implements ReunionDAO {
	Connection connection;
	@Override
	public ReunionTO read(ReunionTO meeting) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ReunionTO> list() {
		List<ReunionTO> result = Collections.EMPTY_LIST;
		Statement stmt;
		try {
			stmt = this.connection.createStatement();
			String sql = "SELECT * FROM meetings;";
			ResultSet rst = stmt.executeQuery(sql);
			result = new ArrayList<ReunionTO>();
			while (rst.next())
			{
				ReunionTO reunion = new ReunionTO();
				reunion.setId(rst.getString("id"));
				reunion.setFechaPlaneada(rst.getDate("fecha"));
				reunion.setMotivo(rst.getString("motivo"));
				result.add(reunion);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public int save(ReunionTO meeting) {
		int retorno = 0;
		String sql = "INSERT INTO meetings(id, motivo, fecha, duracion) VALUES(?,?,?,?)";
		try {
			PreparedStatement pstmt = this.connection.prepareStatement(sql);
			pstmt.setString(1, meeting.getId());
			pstmt.setString(2, meeting.getMotivo());
			pstmt.setDate(3, new Date(meeting.getFechaPlaneada().getTime()));
			pstmt.setLong(4, meeting.getDuracion());
			retorno = pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return retorno;
	}

	@Override
	public int delete(ReunionTO meeting) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setConexion(Connection conexion) {
		this.connection = conexion;
		System.out.println("Conexion configurada...");
	}

	@Override
	public List<ReunionTO> buscarPorFechaPlaneada(java.util.Date inicio) {
		List<ReunionTO> result = Collections.EMPTY_LIST;
		Statement stmt;
		try {
			String sql = "SELECT * FROM meetings WHERE fecha > ? and ";
			stmt = this.connection.prepareStatement(sql);
			ResultSet rst = stmt.executeQuery(sql);
			while (rst.next())
			{
				ReunionTO reunion = new ReunionTO();
				reunion.setId(rst.getString("id"));
				reunion.setFechaPlaneada(rst.getDate("fecha"));
				reunion.setMotivo(rst.getString("motivo"));
				result.add(reunion);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}


}
