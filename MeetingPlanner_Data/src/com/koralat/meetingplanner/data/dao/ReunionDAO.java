package com.koralat.meetingplanner.data.dao;

import java.sql.Connection;
import java.util.Date;
import java.util.List;

import com.koralat.meetingplanner.support.to.ReunionTO;

public interface ReunionDAO {
	public ReunionTO read(ReunionTO meeting);
	public List<ReunionTO> list();
	public int save(ReunionTO meeting);
	public int delete(ReunionTO meeting);
	void setConexion(Connection conexion);
	public List<ReunionTO> buscarPorFechaPlaneada(Date inicio);
}

