package com.koralat.planeador.web;

import javax.faces.bean.ManagedBean;
import javax.faces.event.ActionEvent;

import com.koralat.meetingplanner.business.bc.ReunionBusiness;
import com.koralat.meetingplanner.business.bc.impl.ReunionBusinessDefault;
import com.koralat.meetingplanner.support.to.ReunionTO;


@ManagedBean
public class ReunionManager {

	private ReunionTO reunionData;	
	
    public void salvar(ActionEvent evento){
    	ReunionBusiness reunionBC = new ReunionBusinessDefault();
    	ReunionTO reunion = new ReunionTO();
    	reunion.setFechaPlaneada(reunionData.getFechaPlaneada());
    	reunion.setDuracion(reunionData.getDuracion());
    	reunionBC.nuevaReunion(reunion);
    }
    
    public void setReunionData(ReunionTO reunion){
    	reunionData = reunion;
    }
    
    public ReunionTO getReunionData(){
    	return this.reunionData;
    }
    

}
